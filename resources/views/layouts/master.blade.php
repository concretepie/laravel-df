
<!doctype html>

<html lang="{{ app()->getLocale() }}">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel @yield('title')</title>
        <!-- Fonts -->
       
        <!-- Styles -->
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    </head>
    <body>

     
        
            <div class="container">
                <div class="row">
                    <div class="col">
                      @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif
                    </div>
                </div>      
                <div class="row">
                    <div class="col">
                        <h1>Ruander OOP - Laravel</h1>
                    </div>
                </div> 
                <section class="row content">
                    <div class="col">
                       @yield('content')
                    </div>
                </section> 
            </div>

    </body>
</html>