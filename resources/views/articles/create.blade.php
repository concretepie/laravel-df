
@extends('layouts.app')

@section('title','| új cikk felvitel')
@section('content')

   
    <div class='col-md-8'>
        
      <h2>Új cikk felvitele</h2>
        
     {!! Form::open() !!}
      {{ Form::label('title','Cím:')}}
      {{ Form::text('title','',array('class'=>'form-control'))}}
      
      {{ Form::label('author','Szerző:')}}
      {{ Form::text('author','',array('class'=>'form-control'))}}
      
      {{ Form::label('body','Cikk szövege:')}}
      {{ Form::textarea('body','',array('class'=>'form-control'))}}
      
      
      {{ Form::label('publish_on','Feltöltés dátuma:')}}
      {{ Form::date('publish_on', \Carbon\Carbon::now())}}

      {!! Form::submit("Mehet") !!}

     {!! Form::close() !!}<br>
     </div>
@endsection