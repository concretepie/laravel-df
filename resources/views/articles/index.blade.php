
@extends('layouts.app')

@section('title','| Articles');

@section('content')

@foreach($articles as $article)

<article>

    <h1><a href=#"{{$article->title}}></a></h1>

    <time>{{$article->publish_on}}</time> | <span>{{$article->author}}</span>

    <section>{{$article->body}}</section>

</article>
@endforeach

@endsection