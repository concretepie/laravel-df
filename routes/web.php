<?php

/*

|--------------------------------------------------------------------------

| Web Routes

|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

*/


Route::get('/', function () {

    return view('layouts.master');

});

Route::get('articles', 'ArticlesController@index')->name('articles');

Route::get('articles/create', 'ArticlesController@create');

Route::post('articles/create', 'ArticlesController@store');

    

Route::get('create-dummy-articles/{qty?}', 'ArticlesController@createDummy');
//Route::get('test', function(){

   //$name = 'Dóró <b>Flóra</b>';

   //return view('about')->with('name',$name)->with('date',date("Y-m-d"));

//});



Auth::routes();

Route::get('home', 'HomeController@index')->name('home');

