<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use Faker;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::all();
        //dump és utána die
        //dd($articles->toArray());
        return view('articles.index', compact("articles"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createdummy ($qty=1){
       //dummy cikk generátor
       $faker = Faker\Factory::create('hu_HU');
               for ($i=0; $i < $qty; $i){
                 // echo $faker->name, "\n";
                  $data = [
                      'title'      => $faker->sentence(),
                      'body'       => $faker->paragraph(3, true),
                      'author'     =>$faker->name(),
                      'publish_on' =>$faker->dateTimeInInterval('-7 days', '7 days'),
                  ];
                  //dump($data);
                  $article = new Article($data);
                  $article->save();
               }
               echo "created $qty artcile(s)";
    }
    public function create()
    {
        return view('articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'title' => 'required|max:191',
            'body' => 'required',
            'author' => 'required|max:100',
            'publish_on' => 'required|date'
        ));
        
        $article = new Article;
        
        $article->title = $request->title;
        $article->body = $request->body;
        $article->author = $request->author;
        $article->publish_on = $request->publish_on;
        $article->save();
        
        return redirect()->route('articles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}